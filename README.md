# <Composition story>
**To read**: git@gitlab.com:esmailalmoshki/composition-story.git

**Estimated reading time**: <5 min.>

## Story Outline
this story is mainly written to Illustrate the major differences between compositions and inheritance 
so that the reader afterwards has no misconception in such matter.
  
## Story Organization
**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: <useCompositions / Composition over Inheritance>
